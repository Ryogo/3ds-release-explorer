@ECHO off
SETLOCAL
	SET xProjectName=3DS Releases Explorer
	SET msBuildDirPath="%programfiles(x86)%\MSBuild\12.0\Bin"
	SET WorkDirPath=%~dp0
	SET ContribPath=%WorkDirPath%\contrib
	SET OutPath=%WorkDirPath%OUT
	SET TargetFilePath=%OutPath%\%xProjectName%.exe
	RD "%OutPath%" /S /Q
	
	CALL %msBuildDirPath%\msbuild.exe "%WorkDirPath%\%xProjectName%\%xProjectName%.csproj" /p:Configuration=Release,Platform="Any CPU",OutputPath="%OutPath%" /t:Clean,Build
	FOR /F "delims=" %%G IN ('dir /b /s "%OutPath%\*.dll"') DO ("%ContribPath%\ILRepack.exe" /internalize /out:"%TargetFilePath%" "%TargetFilePath%" "%%G")
	FOR /F "delims=" %%G IN ('dir /b /s "%OutPath%\*.dll"') DO (DEL "%%G")