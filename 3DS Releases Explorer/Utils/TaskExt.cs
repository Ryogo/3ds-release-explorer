﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Void.ThreeDs.RelExp.Utils {
    static class TaskExt {
        public static Task StartAndReturn(this Task task) {
            task.Start();
            return task;
        }

        public static Task<T> StartAndReturn<T>(this Task<T> task) {
            task.Start();
            return task;
        }
    }
}
