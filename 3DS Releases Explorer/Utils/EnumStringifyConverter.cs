﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Void.ThreeDs.RelExp.Utils {
    class EnumStringifyConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            return GetDescription((Enum)value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            throw new NotImplementedException();
        }

        string GetDescription(Enum value) {
            var type = value.GetType();
            var prop = type.GetField(value.ToString());
            var descAttr = prop.GetCustomAttributes(false).FirstOrDefault(x => x.GetType() == typeof(DescriptionAttribute)) as DescriptionAttribute;

            return descAttr == null ? value.ToString() : descAttr.Description;
        }
    }
}
