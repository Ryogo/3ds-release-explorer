﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Void.ThreeDs.RelExp.Utils {
    class Logger {
        string filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "error.log");

        public void Trim() {
            if (!File.Exists(filePath)) { return; }

            var fileInfo = new FileInfo(filePath);
            if (fileInfo.Length < 128 * 1024) { return; }
            try {
                File.Delete(filePath);
            } catch { }
        }

        public void Write(string message) {
            try {
                File.AppendAllText(filePath, string.Format("[{0:dd.MM.yyyy HH:mm}] >> {1}{2}", DateTime.Now, message, Environment.NewLine), Encoding.UTF8);
            } catch { }
        }
    }
}
