﻿using System;
using System.Windows;
using System.Runtime.ExceptionServices;
using Void.ThreeDs.RelExp.Utils;

namespace Void.ThreeDs.RelExp {
    public partial class App : Application {
        Logger log = new Logger();

        public App() {
            AppDomain.CurrentDomain.FirstChanceException += OnException;
            log.Trim();
        }

        void OnException(object sender, FirstChanceExceptionEventArgs e) {
            log.Write(e.Exception.ToString());
        }
    }
}