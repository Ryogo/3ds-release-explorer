﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Void.ThreeDs.RelExp.Models;

namespace Void.ThreeDs.RelExp.Logics {
    class RelXmlParser {
        public IEnumerable<Release> Parse(string xml) {
            var relCollectionString = GetTagStringValue(xml, "releases").First();
            foreach (var relString in GetTagStringValue(relCollectionString, "release")) {
                var release = new Release();
                release.Id = GetTagTagLongValue(relString, "id").First();
                release.Title = GetTagStringValue(relString, "name").First();
                release.Publisher = GetTagStringValue(relString, "publisher").First();
                Region region;
                if (Enum.TryParse<Region>(GetTagStringValue(relString, "region").First(), out region)) {
                    release.Region = region;
                } else {
                    release.Region = Region.Unknown;
                }
                release.Languages = GetTagStringValue(relString, "languages").First();
                release.Group = GetTagStringValue(relString, "group").First();
                release.ImageSize = GetTagTagLongValue(relString, "imagesize").First();
                release.Serial = GetTagStringValue(relString, "serial").First();
                release.TitleId = GetTagStringValue(relString, "titleid").First();
                release.ImgCRC = GetTagStringValue(relString, "imgcrc").First();
                release.FileName = GetTagStringValue(relString, "filename").First();
                release.ReleaseName = GetTagStringValue(relString, "releasename").First();
                release.TrimmedSize = GetTagTagLongValue(relString, "trimmedsize").First();
                release.Firmware = GetTagStringValue(relString, "firmware").First();
                release.Type = (ReleaseType)Enum.Parse(typeof(ReleaseType), GetTagStringValue(relString, "type").First());

                yield return release;
            }
        }

        IEnumerable<string> GetTagStringValue(string xml, string tagName) {
            var regexString = string.Format("<{0}>(?<value>.*?)</{0}>", tagName);
            var matches = Regex.Matches(xml, regexString, RegexOptions.Singleline | RegexOptions.IgnoreCase);

            return matches.Cast<Match>().Select(match => match.Groups["value"].Value);
        }

        IEnumerable<long> GetTagTagLongValue(string xml, string tagName) {
            var regexString = string.Format("<{0}>(?<value>.*?)</{0}>", tagName);
            var matches = Regex.Matches(xml, regexString, RegexOptions.Singleline | RegexOptions.IgnoreCase);

            return matches.Cast<Match>().Select(match => long.Parse(match.Groups["value"].Value));
        }
    }
}
