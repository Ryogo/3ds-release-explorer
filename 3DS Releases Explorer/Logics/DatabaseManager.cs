﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using Void.ThreeDs.RelExp.Models;
using Void.ThreeDs.RelExp.Utils;

namespace Void.ThreeDs.RelExp.Logics {
    class DatabaseManager {
        readonly string releasesXmlPath;

        public event Action Updated = () => { };

        public List<Release> Releases { get; private set; }

        public DatabaseManager() {
            releasesXmlPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "releases.xml");
            Releases = Load();
        }

        List<Release> Load() {
            if (!File.Exists(releasesXmlPath)) { return new List<Release>(); }

            using (var stream = File.OpenRead(releasesXmlPath)) {
                var serializer = new DataContractSerializer(typeof(List<Release>), "releases", "");
                return (List<Release>)serializer.ReadObject(stream);
            }
        }

        public void Save() {
            var serializer = new DataContractSerializer(typeof(List<Release>), "releases", "");
            using (var stream = File.Create(releasesXmlPath)) {
                serializer.WriteObject(stream, Releases);
            }
        }

        public Task Update() {
            return new Task(() => {
                byte[] data = null;

                try {
                    data = DownloadXml();
                } catch { }

                if (data == null) { return; }

                var parser = new RelXmlParser();
                var newReleases = parser.Parse(Encoding.UTF8.GetString(data)).ToArray();
                if (newReleases.Length <= Releases.Count) { return; }
                if (Releases.Any()) {
                    Releases.AddRange(newReleases.Where(x => x.Id > Releases.Max(z => z.Id)));
                } else {
                    Releases.AddRange(newReleases);
                }
                Updated();
            }).StartAndReturn();
        }

        byte[] DownloadXml() {
            using (var client = new WebClient()) {
                client.Encoding = Encoding.UTF8;
                var dataStr = client.DownloadString("http://3dsdb.com/xml.php");
                return Encoding.UTF8.GetBytes(RemoveInvalidUtf8Chars(dataStr));
            }
        }

        char[] RemoveInvalidUtf8Chars(string xml) {
            return xml.Where(x => XmlConvert.IsXmlChar(x)).ToArray();
        }
    }
}
