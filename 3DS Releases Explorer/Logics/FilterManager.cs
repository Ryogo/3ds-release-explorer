﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using Void.ThreeDs.RelExp.Models;

namespace Void.ThreeDs.RelExp.Logics {
    class FilterManager {
        DatabaseManager db;

        public event Action Updated = () => { };

        public ICollectionView Releases { get; private set; }

        public IEnumerable<Region> Regions {
            get {
                var regions = db.Releases.Select(x => x.Region).Distinct();
                return new[] { Region.Any }.Concat(regions).OrderBy(x => x);
            }
        }
        public IEnumerable<string> Languages {
            get {
                var languages =  db.Releases.SelectMany(x => x.Languages.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)).Distinct();
                return new[] { "Any" }.Concat(languages).OrderBy(x => x);
            }
        }
        public IEnumerable<ReleaseType> Types {
            get { 
                var types = db.Releases.Select(x => x.Type).Distinct();
                return new[] { ReleaseType.Any }.Concat(types).OrderBy(x => x);
            }
        }

        string filterValue;
        public string FilterValue {
            get { return filterValue; }
            set {
                filterValue = value;
                Refresh();
            }
        }

        Region selectedRegion;
        public Region SelectedRegion {
            get { return selectedRegion; }
            set { 
                selectedRegion = value;
                Refresh();
            }
        }

        string selectedLanguage;
        public string SelectedLanguage {
            get { return selectedLanguage; }
            set { 
                selectedLanguage = value;
                Refresh();
            }
        }

        ReleaseType selectedType;
        public ReleaseType SelectedType {
            get { return selectedType; }
            set { 
                selectedType = value;
                Refresh();
            }
        }

        public FilterManager(DatabaseManager manager) {
            this.db = manager;
            manager.Updated += OnReleasesUpdated;

            SelectedRegion = Region.Any;
            SelectedLanguage = "Any";
            SelectedType = ReleaseType.Any;
            Releases = CollectionViewSource.GetDefaultView(manager.Releases);
            Releases.Filter = item => {
                var release = (Release)item;
                if (!string.IsNullOrEmpty(FilterValue) && !release.Title.ToLower().Contains(FilterValue.ToLower()) && !release.Serial.ToLower().Contains(FilterValue.ToLower())) { return false; }
                if (SelectedRegion != Region.Any && release.Region != SelectedRegion) { return false; }
                if (SelectedLanguage != "Any" && !release.Languages.Contains(SelectedLanguage)) { return false; }
                if (SelectedType != ReleaseType.Any && release.Type != SelectedType) { return false; }
                return true;
            };
        }

        void OnReleasesUpdated() {
            Refresh();
            Updated();
        }

        void Refresh() {
            if (Releases == null) { return; }
            Application.Current.Dispatcher.BeginInvoke((Action)Releases.Refresh, null);
        }
    }
}