﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Void.ThreeDs.RelExp.Logics;
using Void.ThreeDs.RelExp.Models;

namespace Void.ThreeDs.RelExp.ViewModels {
    class MainViewModel : ViewModelBase {
        DatabaseManager db;
        FilterManager filters;

        public ICollectionView Releases {
            get { return filters.Releases; }
        }

        #region Filters
        public IEnumerable<Region> Regions {
            get { return filters.Regions; }
        }

        public IEnumerable<string> Languages {
            get { return filters.Languages; }
        }

        public IEnumerable<ReleaseType> Types {
            get { return filters.Types; }
        }

        public string FilterValue {
            get { return filters.FilterValue; }
            set { filters.FilterValue = value; }
        }

        public Region SelectedRegion {
            get { return filters.SelectedRegion; }
            set { filters.SelectedRegion = value; }
        }

        public string SelectedLanguage {
            get { return filters.SelectedLanguage; }
            set { filters.SelectedLanguage = value; }
        }

        public ReleaseType SelectedType {
            get { return filters.SelectedType; }
            set { filters.SelectedType = value; }
        }
        #endregion

        bool canUpdate;
        public bool CanUpdate {
            get { return canUpdate; }
            set { SetProperty(ref canUpdate, value, () => CanUpdate); }
        }

        public IList SelectedReleases { get; set; }

        public ICommand Update { get; private set; }
        public ICommand SetState { get; private set; }
        public ICommand CopyTitle { get; private set; }

        public MainViewModel() {
            db = new DatabaseManager();
            filters = new FilterManager(db);
            filters.Updated += OnFiltersUpdated;
            Update = new RelayCommand(OnUpdate);
            SetState = new RelayCommand(OnSetState);
            CopyTitle = new RelayCommand(OnCopyTitle, x => SelectedReleases.Count == 1);
            OnUpdate();
        }

        void OnUpdate(object arg = null) {
            CanUpdate = false;
            db.Update().ContinueWith(task => {
                CanUpdate = true;
                db.Save();
            });
        }

        void OnSetState(object arg = null) {
            foreach (Release release in SelectedReleases) {
                release.State = (ReleaseState)arg;
            }
            db.Save();
        }

        void OnCopyTitle(object arg = null) {
            var release = (Release)SelectedReleases[0];
            Clipboard.SetText(release.Title);
        }

        void OnFiltersUpdated() {
            Notify(() => Regions);
            Notify(() => Languages);
            Notify(() => Types);
        }
    }
}
