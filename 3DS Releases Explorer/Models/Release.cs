﻿using System;
using System.Runtime.Serialization;

namespace Void.ThreeDs.RelExp.Models {
    [DataContract(Name="release", Namespace="")]
    public class Release : ViewModelBase {
        [DataMember(Name = "id", Order = 1)]
        public long Id { get; set; }

        [DataMember(Name = "name", Order = 2)]
        public string Title { get; set; }

        [DataMember(Name = "publisher", Order = 3)]
        public string Publisher { get; set; }

        [DataMember(Name = "region", Order = 4)]
        string regionString;
        public Region Region {
            get { return (Region)Enum.Parse(typeof(Region), regionString); }
            set { regionString = value.ToString(); }
        }

        [DataMember(Name = "languages", Order = 5)]
        public string Languages { get; set; }

        [DataMember(Name = "group", Order = 6)]
        public string Group { get; set; }

        [DataMember(Name = "imagesize", Order = 7)]
        public long ImageSize { get; set; }

        [DataMember(Name = "serial", Order = 8)]
        public string Serial { get; set; }

        [DataMember(Name = "titleid", Order = 9)]
        public string TitleId { get; set; }

        [DataMember(Name = "imgcrc", Order = 10)]
        public string ImgCRC { get; set; }

        [DataMember(Name = "filename", Order = 11)]
        public string FileName { get; set; }

        [DataMember(Name = "releasename", Order = 12)]
        public string ReleaseName { get; set; }

        [DataMember(Name = "trimmedsize", Order = 13)]
        public long TrimmedSize { get; set; }

        [DataMember(Name = "firmware", Order = 14)]
        public string Firmware { get; set; }

        [DataMember(Name = "type", Order = 15)]
        int releaseType;
        public ReleaseType Type {
            set { releaseType = (int)value; }
            get { return (ReleaseType)releaseType; }
        }

        [DataMember(Name = "state", Order = 101)]
        ReleaseState state;
        public ReleaseState State {
            get { return state; }
            set { SetProperty(ref state, value, () => State); }
        }
    }
}