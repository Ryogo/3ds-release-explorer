﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Void.ThreeDs.RelExp.Models {
    public class RelayCommand : ICommand {
        readonly Action<object> cmd;
        readonly Predicate<object> canExecute;

        public event EventHandler CanExecuteChanged {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public RelayCommand(Action<object> cmd) : this(cmd, null) { }

        public RelayCommand(Action<object> cmd, Predicate<object> canExecute) {
            if (cmd == null) { throw new ArgumentNullException("execute"); }

            this.cmd = cmd;
            this.canExecute = canExecute;
        }

        [DebuggerStepThrough]
        public bool CanExecute(object parameter) {
            return canExecute == null ? true : canExecute(parameter);
        }

        public void Execute(object parameter) {
            cmd(parameter);
        }
    }
}