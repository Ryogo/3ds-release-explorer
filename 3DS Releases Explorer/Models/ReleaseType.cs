﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Void.ThreeDs.RelExp.Models {
    public enum ReleaseType {
        Any = 0,
        Game = 1,
        Demo = 2,
        [Description("3DSWare")]
        ThreeDSWare = 3,
        eShop = 4
    }
}
