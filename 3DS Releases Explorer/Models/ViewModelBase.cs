﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Void.ThreeDs.RelExp.Models {
    [DataContract]
    public class ViewModelBase : INotifyPropertyChanged {
        public event PropertyChangedEventHandler PropertyChanged = (x, y) => { };

        protected void Notify<T>(Expression<Func<T>> property) {
            PropertyChanged(this, new PropertyChangedEventArgs(((MemberExpression)property.Body).Member.Name));
        }

        protected void SetProperty<T>(ref T oldValue, T newValue, Expression<Func<T>> property) {
            if (oldValue.Equals(newValue)) { return; }
            oldValue = newValue;
            PropertyChanged(this, new PropertyChangedEventArgs(((MemberExpression)property.Body).Member.Name));
        }
    }
}
