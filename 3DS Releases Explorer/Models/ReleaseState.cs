﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using Void.ThreeDs.RelExp.Utils;

namespace Void.ThreeDs.RelExp.Models {
    public enum ReleaseState {
        None,
        [Description("To Download")]
        ToDownload,
        Owned,
        Ignored
    }
}
