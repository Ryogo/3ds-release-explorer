﻿using System.ComponentModel;

namespace Void.ThreeDs.RelExp.Models {
    public enum Region {
        Unknown = -1,
        Any = 0,
        [Description("Japan")]
        JPN,
        USA,
        [Description("Europe")]
        EUR,
        [Description("Taiwan")]
        TWN,
        [Description("China")]
        CHN,
        [Description("Korea")]
        KOR,
        [Description("France")]
        FRA,
        [Description("Germany")]
        GER,
        [Description("Spain")]
        SPA,
        [Description("Italy")]
        ITA,
        [Description("United Kingdom")]
        UKV,
        [Description("Netherlands")]
        NLD,
        [Description("Russia")]
        RUS
    }
}
