# 3DS Releases Explorer #

![2015-07-04 15-50-12 3DS Releases Explorer.png](https://bitbucket.org/repo/jrzpdX/images/2340751350-2015-07-04%2015-50-12%203DS%20Releases%20Explorer.png)

An offline client for http://3dsdb.com/ releases database.

LICENSED UNDER: [WTFPL-2.0](https://tldrlegal.com/license/do-wtf-you-want-to-public-license-v2-%28wtfpl-2.0%29)